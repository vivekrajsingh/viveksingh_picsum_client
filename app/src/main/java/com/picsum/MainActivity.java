package com.picsum;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
RecyclerView rv;
    MainAdapter adapter;
    ArrayList<picsumModel>  arrayList=new ArrayList<>();

    private final static String BASE_URL=" https://picsum.photos/list";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv=findViewById(R.id.rv);
        adapter=new MainAdapter(MainActivity.this,arrayList);
        rv.setAdapter(adapter);
        rv.setLayoutManager(new GridLayoutManager(this,2));
        apiCall();
    }

    @Override
    protected void onResume() {
        super.onResume();
        apiCall();
    }

    public static boolean isNetworkAvailable(Context context){
        ConnectivityManager connectivityManager =
                (ConnectivityManager)context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void apiCall(){
        if (isNetworkAvailable(this)){
            StringRequest request=new StringRequest(Request.Method.GET, BASE_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {

                        JSONArray jsonArray=new JSONArray(response);
                        for (int i=0;i<jsonArray.length();i++){
                            picsumModel model=new picsumModel();

                            JSONObject jsonObject= jsonArray.getJSONObject(i);
                            model.setId(jsonObject.getString("id"));
                            model.setAuthor(jsonObject.getString("author"));
                            arrayList.add(model);

                        }
                        adapter.notifyDataSetChanged();



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){

            };
            Singleton.getInstance(this).addToRequestQueue(request);



        }else {
            Toast.makeText(this, "check your intrnrt connection...", Toast.LENGTH_SHORT).show();
        }
    }
}